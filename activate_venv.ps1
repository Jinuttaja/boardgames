
If(! (Test-Path .\venv))
{
    python -m venv venv
    .\venv\Scripts\Activate.ps1
    pip install django
    pip install django-bootstrap4
} else {
    Write-Host "Python virtual env already exists. Remove it to create anew. Activating the old"
    .\venv\Scripts\Activate.ps1
}

