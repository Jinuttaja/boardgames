from django import forms


from .models import Boardgame, Loan

class BoardgameForm(forms.ModelForm):
    class Meta:
        model = Boardgame
        fields = ['name', 'info', 'players', 'genre']
        labels = {'name': 'Name', 'info': 'Info', 'players': 'Players', 'genre': 'Genre'}
        widgets = {'info': forms.Textarea(attrs={'cols': 80, 'rows': 5})}

class LoanForm(forms.ModelForm):
    class Meta:
        model = Loan
        fields = ['loaned']
        labels = {'loaned': 'Want to loan this game?'}

class ReturnForm(forms.ModelForm):
    class Meta:
        model = Loan
        fields = ['loaned']
        labels = {'loaned': 'Want to loan this game?'}