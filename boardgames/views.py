from django.http import request
from django.shortcuts import redirect, render
import datetime
from django.http import Http404
from django.contrib.auth.decorators import login_required

from .models import Boardgame, Loan
from .forms import BoardgameForm, LoanForm, ReturnForm

def index(request):
    """The homepage for Boardgames"""
    return render(request, 'boardgames/index.html')

@login_required
def games(request):
    """show all board games"""
    games = Boardgame.objects.order_by('date_added') #should it be ordered by alphabet??
    context = {'games' : games}
    return render(request, 'boardgames/games.html', context)

@login_required
def game(request, game_id):
    """Show a single game and its data"""
    game = Boardgame.objects.get(id=game_id)
    info = game.info #just one per game
    players = game.players
    owner = game.owner
    loans = game.loan_set.all()
    context = {'game' : game, 'info' : info, 'players' : players, 'owner' : owner, 'loans': loans}
    return render(request, 'boardgames/game.html', context)

@login_required
def new_game(request):
    """Add a new game"""
    if request.method != 'POST':
        #No data submitted; create blank form
        form = BoardgameForm()
    else:
        #POST data submitted, process data
        form = BoardgameForm(data=request.POST)
        if form.is_valid():
            new_game = form.save(commit=False)
            new_game.owner = request.user
            new_game.save()
            return redirect('boardgames:games')
        
    #Display a blank/invalid form
    context = {'form': form}
    return render(request, 'boardgames/new_game.html', context)

@login_required
def new_loan(request, game_id):
    """Loan a certain game"""
    game = Boardgame.objects.get(id=game_id)
    current_user = request.user

    if request.method != 'POST':
        # No data submitted; create a blank form.
        form = LoanForm
    else:
        # POST data submitted; process data.
        form = LoanForm(data=request.POST)
        if form.is_valid():
            gameloan = form.save(commit=False)
            gameloan.loaned_game = game
            gameloan.loaner = current_user
            gameloan.loaned = True
            gameloan.save()
            return redirect('boardgames:new_loan', game.id )
    
    # Display a blank or invalid form
    context = {'game': game, 'form': form}
    return render(request, 'boardgames/new_loan.html', context)

@login_required
def return_loan(request, loan_id):
    """Return a certain game"""
    loan = Loan.objects.get(id=loan_id)
    game = loan.loaned_game

    if request.method != 'POST':
        # No data submitted; create a blank form.
        form = ReturnForm
    else:
        # POST data submitted; process data.
        form = ReturnForm(instance=loan, data=request.POST)
        if form.is_valid():
            gamereturn = form.save(commit=False)
            gamereturn.loaned = False
            gamereturn.loan_ended = datetime.datetime.now()
            gamereturn.save()
            return redirect('boardgames:returned_game', game.id )
    
    # Display a blank or invalid form
    context = {'game': game, 'loan': loan, 'form': form}
    return render(request, 'boardgames/return_loan.html', context)

@login_required
def returned_game(request, game_id):
    """Game has been returned"""
    game = Boardgame.objects.get(id=game_id)   
    return render(request, 'boardgames/returned_game.html', {'game': game})

@login_required
def edit_game(request, game_id):
    """Edit an existing game"""
    game = Boardgame.objects.get(id=game_id)
    

    if request.method != 'POST':
        form = BoardgameForm(instance=game)
    else:
        # POST data submitted; process data.
        form = BoardgameForm(instance=game, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('boardgames:game', game_id=game.id)
    
    context = {'game' : game, 'form' : form }
    return render(request, 'boardgames/edit_game.html', context)

