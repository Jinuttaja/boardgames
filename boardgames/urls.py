"""Defines URL patterns for boardgames"""

from django.urls import path

from . import views

app_name = 'boardgames'
urlpatterns = [
    # Homepage
    path('', views.index, name='index'),
    # Page that shows all the games
    path('games/', views.games, name='games'),
    # Detailed page to show one game and its data
    path('games/<int:game_id>/', views.game, name='game'),
    # Page for adding a new game
    path('new_game/', views.new_game, name='new_game'),
    # Page to show a new loan.
    path('new_loan/<int:game_id>/', views.new_loan, name='new_loan'),
    # Page to handle loan return.
    path('return_loan/<int:loan_id>/', views.return_loan, name='return_loan'),
    # Page to handle loan return.
    path('returned_game/<int:game_id>/', views.returned_game, name='returned_game'),
    # Page to edit exsisting games
    path('edit_game/<int:game_id>/', views.edit_game, name='edit_game'),
]