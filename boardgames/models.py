from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.

class Boardgame(models.Model):
    """Board games that are played"""
    name = models.CharField(max_length=200)
    date_added = models.DateTimeField(auto_now_add=True)
    info = models.CharField(max_length=200)
    players = models.IntegerField()
    genre = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        """Return a string representation on the model"""
        return self.name


# class Info (info about the game, genre, how many players... )
# one info per game?
# Can't these just be attributes of the class Boardgame instead? -Milla


# We don't need this anymore, right? -nina

#class Info(models.Model):
#    # info about the boardgame
#    boardgame = models.ForeignKey(Boardgame, on_delete=models.CASCADE)
#    text = models.TextField()
#    date_added = models.DateTimeField(auto_now_add=True)

# class Loans (who loaned and what and when)
# many loans per game

class Loan(models.Model):
    loaned_game = models.ForeignKey(Boardgame, on_delete=CASCADE)
    loaner = models.ForeignKey(User, on_delete=models.CASCADE)
    loaned = models.BooleanField(default=False)
    text = models.TextField()
    loan_started = models.DateTimeField(auto_now_add=True)
    loan_ended = models.DateTimeField(null=True)
    class Meta:
        verbose_name_plural = 'loans'

    def __str__(self):
         """Return a string representation on the model"""
         return f"{self.text[:50]}"
    
    def is_loaned(self):
        if self.loaned == True:
           if self.loan_ended == True:
            return self.loaner
        else:
            return self.loaned_game

    






