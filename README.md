# Boargames

## Activate env

### Windows 

1. Clone repo

2. Enable scripts 

```powershell
Set-ExecutionPolicy Bypass -Scope Process
```

3. If not done create venv

```powershell
.\activate_venv.ps1
```



## Run the server

```powershell
python manage.py runserver
```
